/*
 * Binary to Hex
 * Author: John Merigliano
 * Date: 2/1/2013
 * Description: This program converts a binary number
 * into a hex number.
 */

public class BinaryToHex
{
    // declare the initial string
    String userStr;

    // construct a conversion object
    public BinaryToHex(String userStr)
    {
        // strings are immutable
    }

    // method to format user input and add 0's to the string
    public static String formatInput(String userStr)
    {
        // determine string length
        int len = userStr.length();

        // if len % 4 = .25, there is 1 extra number in the string
        // add 3 zeros at the beginning of the string
        if (len % 4 == 1)
            return "000" + userStr;
        // if len % 4 = .5, there are 2 extra numbers in the string
        // add 2 zeros
        else if (len % 4 == 2)
            return "00" + userStr;
        // if len % 4 = .75, there are 3 extra numbers in the string
        // add 1 zero
        else if (len % 4 == 3)
            return "0" + userStr;
        // otherwise there is no remainder
        // return the original string
        else
        {
            return userStr;
        }
    }
    // method to convert the binary number to a hex number
    public static String binaryToHex(String binaryValue)
    {
        // method will return any or all of the following conditions
        switch (binaryValue)
        {
            case "0000": return "0";
            case "0001": return "1";
            case "0010": return "2";
            case "0011": return "3";
            case "0100": return "4";
            case "0101": return "5";
            case "0110": return "6";
            case "0111": return "7";
            case "1000": return "8";
            case "1001": return "9";
            case "1010": return "A";
            case "1011": return "B";
            case "1100": return "C";
            case "1101": return "D";
            case "1110": return "E";
            case "1111": return "F";
            default: return "invalid input";
        }
    }
// end of BinaryToHex class
}
