#Binary to Hex

This program converts a binary number into a hex value.

For example:

`000100010101 -> 115`

After compiling both files, run the TestBinaryToHex file.

```
$ javac BinaryToHex.java
$ javac TestBinaryToHex.java
$ java TestBinaryToHex

----------------------------------------
    Binary to Hex Converter
----------------------------------------

Enter a series of binary numbers:

000100010101

Binary numbers entered:

000100010101

Conversion to hex: 115
----------------------------------------
```
