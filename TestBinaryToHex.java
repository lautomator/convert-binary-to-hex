/*
 * Binary to Hex
 * Author: John Merigliano
 * Date: 2/1/2013
 * Description: This program converts a binary number
 * into a hex number.
 */

import java.util.Scanner;

public class TestBinaryToHex
{
    /** main method */
    public static void main(String[] args)
    {
        // display title of the program
        System.out.println("\n\n----------------------------------------" +
            "\n\tBinary to Hex Converter" +
            "\n----------------------------------------");

        // create a scanner
        Scanner input = new Scanner(System.in);

        // prompt user for input
        System.out.println("\nEnter a series of binary numbers: ");

        // pass user input into a string
        String userStr = input.nextLine();

        // create a conversion object (c1)
        BinaryToHex c1 = new BinaryToHex(userStr);

        // create a string to store the new string value
        String binaryString = c1.formatInput(userStr);

        // display the results of the conversion
        System.out.print("\nBinary numbers entered:\n"
            + binaryString + "\n\nConversion to hex: ");

        // establish the length of the string
        int len = binaryString.length();

        // create a new string to store a binary value
        String binaryValue = new String();

        // cycle through the string; extract and convert the substrings
        for (int i = 0; i < len; i += 4)
        {
            binaryValue = binaryString.substring(i, i + 4);
            System.out.print(c1.binaryToHex(binaryValue));
        }

        // blank line
        System.out.println("\n\n----------------------------------------\n");


    // end of main method
    }
// end of Test class
}
